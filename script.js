// Carousel
$('.last-ads').slick({
    dots: true,
    infinite: false,
    speed: 300,
    center: true,
    slidesToShow: 3,
    slidesToScroll: 1,
    responsive: [
        {
        breakpoint: 1920,
            settings: {
            slidesToShow: 3,
            slidesToScroll: 1,
            infinite: true,
            dots: true
        }
        },
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1,
          infinite: true,
          dots: true
        }
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
        }
      },
      // You can unslick at a given breakpoint now by adding:
      // settings: "unslick"
      // instead of a settings object
    ]
  });

let btnMenu = document.querySelector('#btn-menu');

btnMenu.addEventListener('click' , function(){
    let iconMenu = document.querySelector('#icon-menu')

    iconMenu.classList.toggle('rotate-90')
})

// Scroll navbar event della funzione si può cancellare
let navbar = document.querySelector('#navbar-presto')

document.addEventListener('scroll', function(event){
    // Se lo scroll da sopra arriva a 500 aggiungimi la classe altrimenti rimuovimi la classe
    if(window.scrollY > 500){
        navbar.classList.add('active')
    } else {
        navbar.classList.remove('active')
    }
})

let categoryWrapper = document.querySelector('#category-wrapper')

function populateCategories(){
    // let categories = ['Moto' , 'Auto' , 'Barche' , 'Giochi' , 'Elettronica' , 'Libri' , 'Telefoni' , 'Vestiti']

    let categories = [
        {name: 'Moto', icon: '<i class="fas fa-motorcycle fs-1"></i>'},
        {name: 'Auto', icon: '<i class="fas fa-car fs-1"></i>'},
        {name: 'Barche', icon: '<i class="fas fa-ship fs-1"></i>'},
        {name: 'Giochi', icon: '<i class="fas fa-gamepad fs-1"></i>'},
        {name: 'Elettronica', icon: '<i class="fas fa-laptop-medical fs-1"></i>'},
        {name: 'Libri', icon: '<i class="fas fa-book-open fs-1"></i>'},
        {name: 'Telefoni', icon: '<i class="fas fa-mobile-alt fs-1"></i>'},
        {name: 'Vestiti', icon: '<i class="fas fa-tshirt fs-1"></i>'},
    ]

    categories.forEach(cat => {
    let card = document.createElement('div')

    card.classList.add('col-12' , 'col-sm-6' , 'col-md-3', 'my-3')
    
    card.innerHTML = `
        <div class="card card-category text-center">
            ${cat.icon}
            <h3 class="fw-bold my-3">${cat.name}</h3>
            <button class="btn btn-outline-accent rounded-pill">Vai alla categoria</button>
        </div>
     `
    categoryWrapper.appendChild(card)
})
}

// Nella pagina annunci non abbiamo il wrapper quindi diciamo che se l'elemento esiste allora mi lanci le funzioni
if(categoryWrapper){
    populateCategories()
}

let lastAdsWrapper = document.querySelector('.last-ads')
function populateLastAds(){
    
    let ads = [
        {title : 'Moto' , description : 'Anteprima descrizione dell\'annuncio' , price : 1230},
        {title : 'Auto rotta' , description : 'Anteprima descrizione dell\'annuncio' , price : 100},
        {title : 'Panda' , description : 'Anteprima descrizione dell\'annuncio' , price : 1500},
        {title : 'Bambola' , description : 'Anteprima descrizione dell\'annuncio' , price : 10},
        {title : 'Raccolta fumetti' , description : 'Antemprima descrizione dell\'annuncio' , price : 50},
        {title : 'Orologio' , description : 'Anteprima descrizione dell\'annuncio' , price : 500},
        {title : 'Iphone 24' , description : 'Anteprima descrizione dell\'annuncio' , price : 1600},
    ]
    // Per ogni elementi ad deve crearmi l'elemento div
    ads.forEach(ad => {
        let card = document.createElement('div')

        card.innerHTML = `
        <div class="card bg-dark text-white mx-2 p-radius">
              <img class="card-img p-radius" src="./img/card.jpg" alt="Card image">
              <div class="card-img-overlay ad-card-overlay p-radius">
                <h5 class="card-title tc-accent fw-bold fs-3">${ad.title}</h5>
                <p class="tc-white">${ad.description}</p>
                <p class="tc-accent">${ad.price} €</p>
                <button class="btn btn-outline-main rounded-pill more">
                  Scopri di più
                </button>
                <button class="btn like tc-main fs-3">
                  <i class="far fa-heart"></i>
                </button>
              </div>
            </div>
        `

        lastAdsWrapper.appendChild(card)
    })
}

if(lastAdsWrapper){
    populateLastAds()
}

// Prendi tutti gli elementi che hanno la classe like
let likebtns = document.querySelectorAll('.like')

// Per ogni elemento btn deve farmi una funzione che mi attiva l'evento ed al click attiva la funzione che attiva o disattiva la classe tc-main ed accent
// Questo codice cosi non funziona perchè non trova i like buttons anche quelli che si trovano nella pagina annunci infatti js funziona in modo particolare perchè il fetch è asincrana cioè la esegue ma non esegue prima tutto la parte prima e poi importa script.js ma esegue tutto in contemporanea quindi per risolvere in annunci.js copio questo copio e lo incollo sotto a populate ads (annunci)
likebtns.forEach(btn => {
    btn.addEventListener('click', () => {
        // Essendo un array e quindi una collezione posso prendere il primo elemento figlio prima diventa fas pieno e poi far quindi vuoto
        btn.children[0].classList.toggle('fas')
        btn.children[0].classList.toggle('far')
        btn.classList.toggle('tc-main')
        btn.classList.toggle('tc-accent')
        
    })
})

