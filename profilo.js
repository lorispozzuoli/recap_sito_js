// Per una chiamata asincrona simile alla ajax usiamo fetch
fetch('./annunci.json').then(data => data.json())
.then(annunci => {

function populateClosed(){
    let table = document.querySelector('#table-closed')
    // Per vedere il numero di annunci presenti
    let counter = document.querySelector('#closedCounter')
    // Stessa cosa per i soldi incassati e sarà la somma di tutti gli annunci
    let earnedcounter = document.querySelector('#earnedCounter')
    // Prendiamo gli annunci per esempio i primi 5
    let ads = annunci.slice(0,5)

    ads.forEach(ad => {
        let tr = document.createElement('tr')

        // Per ogni elemento che cicla fa innerhtml
        tr.innerHTML = `
        <th scope="row">${ad.id}</th>
        <td>${ad.name}</td>
        <td>${ad.category}</td>
        <td>${ad.price} €</td>
        `

        table.appendChild(tr)
    })
    // Riempio la stringa con la lunghezza degli ads della funzione
    counter.innerHTML = ads.length
    // Per la somma di tutti i prezzi usiamo il comando reduce o math.sum ma si usa per gli array quindi facciamo il totale di un elemento deve essere il totale + il prezzo il ogni elemento convertendo dove serve in numeri inserendo un valore di default con , 0 perchè reduce fa partire il totale da un numero
    earnedcounter.innerHTML = ads.reduce((tot , el) => tot + Number(el.price) , 0)
}

populateClosed()

function populateOpened(){
    let table = document.querySelector('#table-opened')
    
    let ads = annunci.slice(9,15)

    ads.forEach(ad => {
        let tr = document.createElement('tr')

        // Per ogni elemento che cicla fa innerhtml
        tr.innerHTML = `
        <th scope="row">${ad.id}</th>
        <td>${ad.name}</td>
        <td>${ad.category}</td>
        <td>${ad.price} €</td>
        `

        table.appendChild(tr)
    })
}

populateOpened()

})

// Scroll navbar event della funzione si può cancellare
let navbar = document.querySelector('#navbar-presto')

document.addEventListener('scroll', function(event){
    // Se lo scroll da sopra arriva a 200 aggiungimi la classe altrimenti rimuovimi la classe
    if(window.scrollY > 200){
        navbar.classList.add('active')
    } else {
        navbar.classList.remove('active')
    }
})