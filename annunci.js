// Per una chiamata asincrona simile alla ajax usiamo fetch
fetch('./annunci.json').then(data => data.json())
.then(annunci => {

    function populateAds(annunci){
        let adsWrapper = document.querySelector('#ads-wrapper')
        // Per svuotarlo
        adsWrapper.innerHTML = ``
        annunci.forEach(ad => {
            let card = document.createElement('div')

            card.classList.add('col-12' , 'col-sm-6', 'col-xl-4', 'my-4')
            // Come nome delle proprietà al posto di title metto name per riprendere i dati nel file json e li cambio anche per gli altri (ad.title -> ad.name) non avendo description lo scrivo a mano
            card.innerHTML = `
            <div class="card bg-dark text-white mx-2 p-radius">
                    <img class="card-img p-radius" src="./img/card.jpg" alt="Card image">
                <div class="card-img-overlay ad-card-overlay p-radius">
                    <h5 class="card-title tc-accent fw-bold fs-3">${ad.name}</h5>
                    <p class="tc-white">Descrizione dell'annuncio con qualche dettaglio aggiuntivo per vedere il prodotto.</p>
                    <p class="tc-accent">${ad.price} €</p>
                    <button class="btn btn-outline-main rounded-pill more" ad-id='${ad.id}' data-bs-toggle="modal" data-bs-target="#adModal">
                        Scopri di più
                    </button>
                    <button class="btn like tc-main fs-3">
                        <i class="far fa-heart"></i>
                    </button>
                </div>
            </div>
            `
            adsWrapper.appendChild(card)
        });

        // Prendi tutti gli elementi che hanno la classe like
        let likebtns = document.querySelectorAll('.like')

        // Per ogni elemento btn deve farmi una funzione che mi attiva l'evento ed al click attiva la funzione che attiva o disattiva la classe tc-main ed accent
        // Questo codice cosi non funziona perchè non trova i like buttons anche quelli che si trovano nella pagina annunci infatti js funziona in modo particolare perchè il fetch è asincrana cioè la esegue ma non esegue prima tutto la parte prima e poi importa script.js ma esegue tutto in contemporanea quindi per risolvere in annunci.js copio questo copio e lo incollo sotto a populate ads (annunci)
        likebtns.forEach(btn => {
        btn.addEventListener('click', () => {
        // Essendo un array e quindi una collezione posso prendere il primo elemento figlio prima diventa fas pieno e poi far quindi vuoto
        btn.children[0].classList.toggle('fas')
        btn.children[0].classList.toggle('far')
        btn.classList.toggle('tc-main')
        btn.classList.toggle('tc-accent')
            
        })
    })
        // Quando vado a filtrare js elimina gli add event lister e dato che non esistono si blocca quindi il populate modal lo lanciamo quando facciamo popupale ads e riaggancia l'evento del click su quel bottone
        populateModal()
    }

    populateAds(annunci)

    function populateFilterCategories(){

        let setCategories = new Set();

        let categoriesWrapper = document.querySelector('#category-filters')
        // Per ogni annuncio aggiungimi un ad.category e mi prende le 9 categorie
        annunci.forEach(ad => {
            setCategories.add(ad.category)
        })
        // Ritrasformiamo setCategories in un array
        setCategories = Array.from(setCategories)

        setCategories.forEach(cat => {
            let input = document.createElement('div')

            input.classList.add('form-check')
            // Creo un attributo personalizzato category filter e ci carico le categorie
            input.innerHTML = `
            <input class="form-check-input" category-filter = '${cat}' type="radio" name="category-filter">
            <label class="form-check-label" for="flexRadioDefault1">
                ${cat}
            </label>
            `

            categoriesWrapper.appendChild(input)
        })
    }

    populateFilterCategories()
    
    function filterCategory(){
        let filters = document.querySelectorAll('[category-filter]')

        filters.forEach(filter => {
            // All'evento di input devi farmi tutto questo
            filter.addEventListener('input' , () => {
                let query = filter.getAttribute('category-filter')
                // Sto filtrando per tutti gli annunci che hanno come categoria query

                // Se la ricerca è uguale a tutti gli annunci allora mi dai gli annunci altrimenti solo quelli filtrati
                let filtered;
                if(query === 'All'){
                    filtered = annunci
                } else {
                    filtered = annunci.filter(ad => ad.category === query)
                }
                // Ora questo filtro deve anche ordinare
                let sortedByPrice = changePriceOrder(filtered)
                let sorted = changeDateOrder(sortedByPrice)
                // populateAds(filtered)
                populateAds(sorted)
            })
        })
    }

    filterCategory()

    function filterSearch(){
        let searchInput = document.querySelector('#input-search')

        searchInput.addEventListener('input' , () => {
            let search = searchInput.value.toLowerCase()
            // Filtrami se alla parola dell'annuncio c'è search
            let filtered = annunci.filter(ad => ad.name.toLowerCase().includes(search))

            populateAds(filtered)
        })
    }

    filterSearch()

    function searchOnLoad(){
        // Se c'è qualcosa nell'uri allora fai tutto altrimenti no
        if(location.search){
            let search = location.search.slice(7).toLowerCase()

            let searchInput = document.querySelector('#input-search').value = search

            let filtered = annunci.filter(ad => ad.name.toLowerCase().includes(search))

            populateAds(filtered)
        }
        
    }

    searchOnLoad()

    // Per fare il filtro per prezzo devo trovare il prezzo minimo (0) ed il massimo in modo dinamico
    function populatePriceRange(){
        // Ci catturiamo gli annunci e li facciamo ciclare e per ogni ad dobbiamo fare un controllo ma da un problema perchè i prezzi sono in stringhe con i codici ascii e dobbiamo trasformare ad.price in un numero
        let maxPrice = 0
        let maxLabel = document.querySelector('#max-price-label')
        // Avendo cambiando il tipo di input imposto minimo e massimo
        let inputPriceMax = document.querySelector('#price-max-input')
        let inputPriceMin = document.querySelector('#price-min-input')
        // Inseriamo un altro label sotto l'input per vedere in che prezzo mi trovo e devo attaccare l'evento
        let selectedPrice = document.querySelector('#selected-price')


        annunci.forEach(ad => {
            if(Number(ad.price) > Number(maxPrice)){
                maxPrice = ad.price
            }
        })

        maxLabel.innerHTML = Math.ceil(maxPrice) + ' €'

        // Prendo la variabile e setto l'attributo max ed arrotondo per eccesso
        inputPriceMax.setAttribute('max' , Math.ceil(maxPrice) )
        // Dato che devo impostare al massimo il valore imposto l'input in modo che il pallino si trova sempre al massimo
        inputPriceMax.value = maxPrice

        // Avendo preso un input con min e max facciamo la stessa cosa con min 
        inputPriceMin.setAttribute('max' , maxPrice)
        // Metto 0 per farlo partire all'inzio
        inputPriceMin.value = 0

        selectedPrice.innerHTML = maxPrice + ' €'
        
    }

    populatePriceRange()

    // Una volta impostato l'input che indica in che prezzo ci troviamo dobbiamo creare la funzione per filtrare il prezzo
    function filterPrice(){
        // Prendo le variabili creati nell'altra funzione
        let selectedPrice = document.querySelector('#selected-price')
        // Avendo cambiando il tipo di input imposto minimo e massimo e devo calcolare entrambi
        let inputPriceMax = document.querySelector('#price-max-input')
        let inputPriceMin = document.querySelector('#price-min-input')
        // Aggiungi un evento che lo ascolta e quindi l'input per far variare il prezzo quando uso la barra
        inputPriceMax.addEventListener('input' , () => {
            
            // Per fare bloccare l'input quando si incontra facciamo un if se il massimo diventa minore del minimo il minimo diventa dello stesso valore del massimo per spostarli insieme con un range di 200
            if((Number(inputPriceMax.value) - 200) < Number (inputPriceMin.value)){
                inputPriceMin.value = Number(inputPriceMax.value) - 200
            }
            
            selectedPrice.innerHTML = `${inputPriceMin.value} € - ${inputPriceMax.value} €`
            // Per filtrare il prezzo filtro gli annunci e li faccio visualizzare e prendo gli annunci minore del massimo sempre trasformandoli in numeri perchè sono stringhe
            // Per filtrare gli annunci e farli visualizzare e devo filtrare anche per i numeri maggiori del minimo
            let filtered = annunci.filter(ad => Number(ad.price) <= Number(inputPriceMax.value)).filter(ad => Number(ad.price) >= Number(inputPriceMin.value))
            
            populateAds(filtered)
        })
        // Faccio la stessa cosa per min in modo che se cambio l'input cambia il prezzo 
        inputPriceMin.addEventListener('input' , () => {
            if((Number(inputPriceMin.value) + 200) > Number (inputPriceMax.value)){
                inputPriceMax.value = Number(inputPriceMin.value) + 200
            }
            selectedPrice.innerHTML = `${inputPriceMin.value} € - ${inputPriceMax.value} €`

            let filtered = annunci.filter(ad => Number(ad.price) <= Number(inputPriceMax.value)).filter(ad => Number(ad.price) >= Number(inputPriceMin.value))
            
            populateAds(filtered)
        })

    }

    filterPrice()

    // Per evitare di ricaricare ma ogni volta creare annunci per rifare tutto possiamo cambiare la funzione changeprice order cioè lasciamo la funzione ma non attacchiamo l'evento al click ma lo mettiamo al filtered quindi l'ordiniamo sia per prezzo che per data in modo che quando vengono scelte le categorie mettono le date oppure mettere un tasto filtra più specifico se abbiamo una parola o un testo dato che lo vogliamo reattivo scegliamo la prima opzione e quindi il changeprice order lo mettiamo nella funzione di filtro e togliamo l'evento
    function changePriceOrder(ads){
        let orderPriceCheckbox = document.querySelector('#orderPrice')

        // orderPriceCheckbox.addEventListener('input' , () => {
            // L'if deve prendere gli annunci filtrari ed ordinarli
            if(orderPriceCheckbox.checked){
                let sorted = ads.sort((a , b) => Number(a.price) - Number(b.price))

                // populateAds(sorted)
                return sorted
            } else {
                let sorted = ads.sort((a , b) => Number(b.price) - Number(a.price))

                // populateAds(sorted)
                return sorted
            }
        // })
    }
    // Ora la funzione la lancio internamente
    // changePriceOrder()

    
    // Ordiniamo gli annunci in ordine decrescente e crescente sia per data che per prezzo, per ordinarli prenderemo degli ads
    function changeDateOrder(ads){
        let orderDateCheckbox = document.querySelector('#orderDate')

        // orderDateCheckbox.addEventListener('input' , () => {
            if(orderDateCheckbox.checked){
                let sorted = ads.sort((a , b) => Number(a.id) - Number(b.id))

                // populateAds(sorted)
                return sorted

            } else {
                let sorted = ads.sort((a , b) => Number(b.id) - Number(a.id))

                // populateAds(sorted)
                return sorted
            }
        // })
    }
    // Ora la funzione la lancio internamente come fatto per date
    // changeDateOrder()

    function populateModal(){
        // Ci prendiamo tutti i bottoni che hanno come data attributi ad-id e li cicliamo

        let btns = document.querySelectorAll('[ad-id]')

        btns.forEach(btn => {
            btn.addEventListener('click' , () => {
                // Una volta cliccato devo trovare l'annuncio ed il suo array per recuperare i suoi dati
                let id = btn.getAttribute('ad-id')

                let ad = annunci.find(el => el.id == id)

                // Popoliamo tutti gli id messi nell'html
                let MTitle = document.querySelector('#adModalTitle')
                let MPrice = document.querySelector('#adModalPrice')
                let MCategory = document.querySelector('#adModalCategory')

                // Per ognuno li riempiamo
                MTitle.innerHTML = ad.name
                MPrice.innerHTML = ad.price
                MCategory.innerHTML = ad.category
            })
        })
    }
})

// Scroll navbar event della funzione si può cancellare
let navbar = document.querySelector('#navbar-presto')

document.addEventListener('scroll', function(event){
    // Se lo scroll da sopra arriva a 500 aggiungimi la classe altrimenti rimuovimi la classe
    if(window.scrollY > 500){
        navbar.classList.add('active')
    } else {
        navbar.classList.remove('active')
    }
})
